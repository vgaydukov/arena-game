import tornado.web
from tornado.options import options
import os

from controllers.base import BaseController

class Levels(BaseController):

    def get(self):
        if options.debug:
            self.write_json({ "levels": self.collect_levels()})
        else:
            self.write("Only in debug mode")

    def collect_levels(self):

        directory = "static/assets/levels"
        files = os.listdir(directory)
        levels = []

        for file in files:
        	if file.endswith("json"):
        		file = "levels/" + file
        		levels.append(file)
        return levels 


