import tornado.web
import uuid

from controllers.base import BaseController

class Index(BaseController):

    def get(self):
        if not self.current_user:
            self.set_secure_cookie('secret', str(uuid.uuid4()));
        self.render("index.html")
        
