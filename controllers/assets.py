import tornado.web
from tornado.options import options
import os

from controllers.base import BaseController

class Assets(BaseController):

    def get(self):
        if options.debug:
            self.write_json({ "assets": self.collect_assets()})
        else:
            self.write("Only in debug mode")


    def collect_files(self, dir, target, types, prefix):
        files = os.listdir(dir)

        for file in files:
            if file.endswith(types):
                file = prefix + file
                target.append(file)
        return target 

    def collect_assets(self):

        assets = []

        self.collect_files("static/assets/models", assets, (".json", ".obj", ".dae"), "models/")
        self.collect_files("static/assets/images", assets, (".jpg", ".png"), "images/")

        return assets 


