import json
import tornado.web
import tornado.escape

class BaseController(tornado.web.RequestHandler):
    
    def get_current_user(self):
        return self.get_secure_cookie('secret')
        
    def prepare(self):
        try:
            if self.request.headers.get("Content-Type") and self.request.headers.get("Content-Type").startswith("application/json"):
                self.args = json.loads(self.request.body)
            else:
                self.args = { k: self.get_argument(k) for k in self.request.arguments }
            self.args.pop('_xsrf')
        except:
            self.args = None
    
    def error_message(self, name, val=None):
        error = {
            'error': True,
        }
        self.write(tornado.escape.json_encode(error))
    
    def write_json(self, data):
        self.write(tornado.escape.json_encode(data))