import tornado.web
from tornado.options import options

from controllers.base import BaseController

class Editor(BaseController):

    def get(self):
        if options.debug:
            self.render("level_editor.html")
        else:
            self.write("Only in debug mode")
