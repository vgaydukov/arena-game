import tornado.web
import uuid

from controllers.base import BaseController

class Room(BaseController):

    def get(self):
        if self.current_user:
            self.write_json({
            	"room": {
            		"address": "10.10.10.10",
                    "level": "level (4)",
                    "players": [
                        {
                            "tank": "pioneer",
                            "local": True,
                            "id": "11111-11111",
                            "weapon": "gun"
                        },
                        {
                            "tank": "pioneer",
                            "id": "22222-22222",
                            "weapon": "gun"
                        },
                    ]
            	}
            })
