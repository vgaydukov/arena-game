import tornado.web
import uuid

from controllers.base import BaseController

class User(BaseController):

    def get(self):
        if self.current_user:
            self.write_json({
            	"user": {
            		"characters": [
            			{
                            "class": "Wizard", 
                            "lvl": 1,
                            "speed": 111
                        },
            		],
            		"gold": 1000,
            		"gems": 100
            	}
            });
        
