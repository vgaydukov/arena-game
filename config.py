from tornado.options import define

define("port", default=8888, help="run on the given port", type=int)
define("debug", default=False, help="run in debug mode", type=int)
define("users_secret", default="WGS3U4AGHCJII3CMS58RBCZ94P79NU6C9L954JIFMB85VZCR30W9IRMHPDNZXYMD", type=str)
define("cookie_secret", default="22G9B8TT70A0WH0MHWNXCU140CLL9QDVWBYLZPO7QGRRN79F5I5B3G55T132J3SU", type=str)