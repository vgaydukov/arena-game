
require("jquery-ui");
require("jquery-ui/ui/widgets/button");
require("jquery-ui/ui/widgets/selectmenu");
require("jquery-ui/ui/widgets/dialog");
require("jquery-ui/ui/widgets/progressbar");
require("jquery-ui/ui/widgets/spinner");

import {LevelEditor} from "./editor/js/LevelEditor";

window.onload = function() {
    var editor = new LevelEditor("main");
};
