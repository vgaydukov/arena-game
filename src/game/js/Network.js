function Network() {

    VG.EventDispatcher.bind("Network.request", this, this.request);
    VG.EventDispatcher.bind("Network.connectRoom", this, this.connectRoom);

}

Network.prototype = {
    constructor: Network,

    isJson: function(str) {
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    },

    request: function(params) {
        var method = params.method || "GET";
        var url = params.url || null;
        var data = params.data || {};
        var onSuccess = params.onSuccess || null;
        var onError = params.onError || null;

        data._xsrf = $("input[name='_xsrf']").val();

        if (url) {
            $.ajax({
                method: method,
                url: url,
                data: data
            }).done(function(msg) {
                msg = JSON.parse(msg);
                if (msg && msg.error) {
                    onError(msg);
                    return;
                }
                if (onSuccess) onSuccess(msg);
            }).fail(function() {
                onError();
            });
        }
    },

    connectRoom: function() {

    }
};

export {Network};
