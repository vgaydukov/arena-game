import {Network} from "./Network";
import {SceneSplashScreen} from "./scenes/SceneSplashScreen";
import {SceneMainMenu} from "./scenes/SceneMainMenu";
import {SceneBattleRoom} from "./scenes/SceneBattleRoom";
import {DOM_ELEMENT, STATIC_PATH, PACK_PATH, LEVELS_PATH} from "./settings";

function Game() {

    VG.SETTINGS.CLEAR_COLOR = "black";

    var engine = new VG.Engine("main");
    engine.renderer.shadowMap.enabled = true;

    var mouse = new VG.MouseEventsHandler(DOM_ELEMENT);
    var keyboard = new VG.KeyboardEventsHandler(document);

    var network = new Network();

    this.sceneController = new VG.SceneController();
    engine.add(this.sceneController);

    var sceneSplashScreen = new SceneSplashScreen();
    this.sceneController.add(sceneSplashScreen);

    var sceneMainMenu = new SceneMainMenu();
    this.sceneController.add(sceneMainMenu);

    var sceneBattleRoom = new SceneBattleRoom();
    this.sceneController.add(sceneBattleRoom);

    this.assetsLoader = new VG.AssetsLoader(STATIC_PATH + "assets/");

    this.startGame();

    VG.EventDispatcher.bind("Game.findBattleRoom", this, this.findBattleRoom);

}

Game.prototype = {
    constructor: Game,

    findBattleRoom: function() {

        var context = this;

        context.sceneController.activateScene("splash");

        VG.EventDispatcher.send("Network.request", {
            url: "/room",
            onSuccess: function(data) {
                if (data.repeat) {
                    setTimeout(function() { context.findBattleRoom(); }, 5000);
                }
                else {
                    context.createBattleRoom(data);
                }
            }
        });

    },

    createBattleRoom: function(data) {
        var context = this;

        this.sceneController.activateScene("splash");

        this.assetsLoader.loadPack(STATIC_PATH + "assets/" + LEVELS_PATH + data.room.level + ".json",
            null,
            null,
            function(level) {
                data.level = level;
                context.sceneController.activateScene("battle-room", data);
            });
    },

    toMainMenu: function() {
        var context = this;

        context.sceneController.activateScene("splash");

        VG.EventDispatcher.send("Network.request", {
            url: "/user",
            onSuccess: function(data) {
                context.sceneController.activateScene("main-menu", data);
                console.log(data);
            }
        });
    },

    startGame: function() {
        var context = this;

        this.sceneController.activateScene("splash");

        this.assetsLoader.loadPack(STATIC_PATH + "assets/" + PACK_PATH + "preload.json",
            null,
            null,
            function() {
                context.toMainMenu();
            });
    }
};
export {Game};
