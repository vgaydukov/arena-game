import {UISplashScreen} from "../ui/UISplashScreen";

function SceneSplashScreen() {

    VG.Scene.apply(this);

    this.name = "splash";

    this.ui = new UISplashScreen("splash");

}

SceneSplashScreen.prototype = Object.create(VG.Scene.prototype);
SceneSplashScreen.constructor = SceneSplashScreen;

export {SceneSplashScreen};
