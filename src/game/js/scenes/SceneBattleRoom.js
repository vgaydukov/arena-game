import {UIBattleRoom} from "../ui/UIBattleRoom";
import {BattleRoom} from "../game_objects/BattleRoom";

function SceneBattleRoom() {

    VG.Scene.apply(this);

    this.name = "battle-room";

    this.ui = new UIBattleRoom("battle-room");

    this.view = new BattleRoom();
}

SceneBattleRoom.prototype = Object.create(VG.Scene.prototype);
SceneBattleRoom.constructor = SceneBattleRoom;

SceneBattleRoom.prototype.activate = function(data) {

    VG.Scene.prototype.activate.apply(this, arguments);

    this.view.activate(data);

};

SceneBattleRoom.prototype.deactivate = function() {

    VG.Scene.prototype.deactivate.apply(this, arguments);

    this.view.deactivate();

};

SceneBattleRoom.prototype.update = function(dt) {

    VG.Scene.prototype.update.apply(this, arguments);

    this.view.update(dt);

};

export {SceneBattleRoom};
