import {UIMainMenu} from "../ui/UIMainMenu";
import {Tank} from "../game_objects/Tank";

function SceneMainMenu() {

    VG.Scene.apply(this);

    this.name = "main-menu";

    this.ui = new UIMainMenu("main-menu");

}

SceneMainMenu.prototype = Object.create(VG.Scene.prototype);
SceneMainMenu.constructor = SceneMainMenu;

SceneMainMenu.prototype.activate = function(data) {

    var alight = new THREE.AmbientLight("white");
    this.add(alight);

    var dlight1 = new THREE.DirectionalLight("white", 1);
    dlight1.position.set(0, 200, 200);
    dlight1.castShadow = true;
    dlight1.shadow.camera.left = -512;
    dlight1.shadow.camera.right = 512;
    dlight1.shadow.camera.top = 512;
    dlight1.shadow.camera.bottom = -512;
    dlight1.shadow.mapSize.width = 512;
    dlight1.shadow.mapSize.height = 512;
    dlight1.shadow.camera.near = 0.1;
    dlight1.shadow.camera.far = 500;
    //dlight1.shadow.bias = data.dl1shbias;
    this.add(dlight1);

    var dlight2 = new THREE.DirectionalLight("white", 1);
    dlight2.position.set(0, -200, -200);
    this.add(dlight2);

    var camera = VG.EventDispatcher.query("Engine.get.camera");
    camera.position.set(0, 7, 10);
    camera.lookAt(0, 2.6, 0);

    for (var i = 0; i < 1; i++) {

        var char = new Tank({"tank": "pioneer", "weapon": "gun"});
        char.position.set(i * 5, 0, 0);
        this.add(char);

    }

    VG.Scene.prototype.activate.apply(this, arguments);

};

SceneMainMenu.prototype.deactivate = function() {

    VG.Scene.prototype.deactivate.apply(this);

    this.clear();

};

export {SceneMainMenu};
