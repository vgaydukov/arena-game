import {DOM_ELEMENT} from "../settings";

function UIMainMenu() {

    VG.UI.Container.apply(this, arguments);

    DOM_ELEMENT.appendChild(this.view);

}

UIMainMenu.prototype = Object.create(VG.UI.Container.prototype);
UIMainMenu.constructor = UIMainMenu;

UIMainMenu.prototype.show = function() {

    var startBtn = new VG.UI.Button(
        VG.EventDispatcher.query("AssetsLoader.getAsset", "BTN_RED_RECT_OUT"),
        VG.EventDispatcher.query("AssetsLoader.getAsset", "BTN_RED_RECT_IN"),
        function() {
            VG.EventDispatcher.send("Game.findBattleRoom");
        });
    startBtn.scale = 0.5;
    startBtn.position = {x: "50%", y: "92%"};
    this.add(startBtn);

    var startText = new VG.UI.TextElement("Fight", "25px arial, sans-serif", "white");
    startBtn.add(startText);
    startText.position = {x: "50%", y: "52%"};

};

UIMainMenu.prototype.hide = function() {

    this.clear();

};

export {UIMainMenu};
