import {DOM_ELEMENT, STATIC_PATH} from "../settings";

function UISplashScreen() {

    VG.UI.Container.apply(this, arguments);

    var logo = new Image();
    logo.src = STATIC_PATH + "assets/images/SYMB_SETTINGS.png";
    logo.style.position = "absolute";
    logo.style.top = "calc(50% - 40px)";
    logo.style.left = "calc(50% - 40px)";
    logo.style.width = "80px";
    logo.style.height = "80px";

    this.add(logo);

    DOM_ELEMENT.appendChild(this.view);
}

UISplashScreen.prototype = Object.create(VG.UI.Container.prototype);
UISplashScreen.constructor = UISplashScreen;

export {UISplashScreen};
