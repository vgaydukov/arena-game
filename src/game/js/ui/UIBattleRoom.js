import {DOM_ELEMENT} from "../settings";

function UIBattleRoom() {

    VG.UI.Container.apply(this, arguments);

    DOM_ELEMENT.appendChild(this.view);

}

UIBattleRoom.prototype = Object.create(VG.UI.Container.prototype);
UIBattleRoom.constructor = UIBattleRoom;

UIBattleRoom.prototype.show = function() {

    var startBtn = new VG.UI.Button(
        VG.EventDispatcher.query("AssetsLoader.getAsset", "BTN_RED_RECT_OUT"),
        VG.EventDispatcher.query("AssetsLoader.getAsset", "BTN_RED_RECT_IN"),
        function() {

        });
    startBtn.scale = 0.5;
    startBtn.position = {x: "50%", y: 10};
    this.add(startBtn);

    var startText = new VG.UI.TextElement("Click to fight", "25px arial, sans-serif", "white");
    startBtn.add(startText);
    startText.position = {x: "50%", y: "52%"};
};

export {UIBattleRoom};
