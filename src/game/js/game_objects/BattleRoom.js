import {Player} from "./Player";
import {Level} from "./Level";
import {PlayerController} from "./PlayerController";
import {CAMERA_OFFSET, CHAR_STATE, TANK_SPEED} from "../settings";

function BattleRoom() {

    VG.SceneEntity.apply(this, arguments);
    this.matrixAutoUpdate = false;

    this.level = null;

    this.gameObjects = [];

    var camera = VG.EventDispatcher.query("Engine.get.camera");

    this.playerController = new PlayerController();

    this.cameraController = new VG.CameraControllerTopDown({
        camera: camera,
        offset: CAMERA_OFFSET
    });
    this.add(this.cameraController);

    VG.EventDispatcher.bind("BattleRoom.createPlayer", this, this.createPlayer);

}

BattleRoom.prototype = Object.create(VG.SceneEntity.prototype);
BattleRoom.constructor = BattleRoom;

BattleRoom.prototype.createPlayer = function(data) {

    var player = new Player(data);
    this.add(player.character);
    this.gameObjects.push(player);

    if (player.local) {

        this.cameraController.setTarget(player.view);

        this.playerController.setTarget(player);
        this.playerController.activate();
    }

};

BattleRoom.prototype.activate = function(data) {
    this.clear();

    this.level = new Level(data.level);
    this.add(this.level);

    for (var player in data.room.players) {
        this.createPlayer(data.room.players[player]);
    }

};

BattleRoom.prototype.deactivate = function(data) {

    this.clear();
    this.playerController.deactivate();

};

BattleRoom.prototype.clear = function() {

    this.remove(this.level);

    for (var i = this.gameObjects.length - 1; i >= 0; i--) {
        if (this.gameObjects[i].destroy) {
            this.gameObjects[i].destroy();
            this.remove(this.gameObjects[i]);
        }
    }
};

BattleRoom.prototype.update = function(dt) {

    var currentObject = null;
    var objectSpeed = null;

    return function(dt) {

        for (var i = this.gameObjects.length - 1; i >= 0; i--) {

            currentObject = this.gameObjects[i];

            if (currentObject.state == CHAR_STATE.MOVE) {

                currentObject.rotate(dt);

                objectSpeed = this.calculateObjectSpeed(currentObject, dt);
                currentObject.move(objectSpeed);

            }
        }
        VG.SceneEntity.prototype.update.apply(this, arguments);
    };

}();

BattleRoom.prototype.validateObjectPosition = function(position) {

    position = position.clone().addScalar(this.level.size / 2);

    position.x = Math.round(position.x / this.level.step);
    position.z = Math.round(position.z / this.level.step);

    if (this.level.matrix.getCell(position.x, position.z) == 0) {
        return true;
    }

    return false;
};

BattleRoom.prototype.calculateObjectSpeed = function() {

    var v1 = new THREE.Vector3();
    var axis = new THREE.Vector3(0, 0, 1);
    var tempPos = new THREE.Vector3();
    var result = new THREE.Vector3();
    var vx = 0,
        vz = 0;

    return function(gameObject, dt) {

        result.set(0, 0, 0);

        v1.copy(axis).applyEuler(gameObject.rotation);

        vx = (v1.x * TANK_SPEED * dt);

        tempPos.copy(gameObject.position);
        tempPos.x += vx;

        result.x = this.validateObjectPosition(tempPos) ? vx : 0;

        vz = (v1.z * TANK_SPEED * dt);

        tempPos.copy(gameObject.position);
        tempPos.z += vz;

        result.z = this.validateObjectPosition(tempPos) ? vz : 0;

        return result;

    };

}();

export {BattleRoom};
