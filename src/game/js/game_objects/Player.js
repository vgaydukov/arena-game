import {Tank} from "./Tank";
import {ROTATE_ANGLE_STEP, CHAR_STATE} from "../settings";

function lerp(a, b, n) {
    return ((1 - n) * a + n * b);
}

function Player(data) {

    this.id = data.id;
    this.local = data.local;
    this.state = CHAR_STATE.STAND;
    this.character = new Tank(data);

    if (!this.local) {
        VG.EventDispatcher.bind("Player-" + this.id + "-move.start", this, this.moveStart);
        VG.EventDispatcher.bind("Player-" + this.id + "-move.end", this, this.moveEnd);
        VG.EventDispatcher.bind("Player-" + this.id + "-rotation.set", this, this.rotationSet);
    }

}

Player.prototype = {

    constructor: Player,

    rotation: new THREE.Euler(),

    get view() {
        return this.character.view;
    },

    get position() {
        return this.character.position;
    },

    moveStart: function() {

        this.state = CHAR_STATE.MOVE;
    },

    moveEnd: function() {

        this.state = CHAR_STATE.STAND;

    },

    attack: function() {

        this.state = CHAR_STATE.ATTACK;

    },

    rotationSet: function(rotation) {

        rotation = (Math.round(Math.abs(rotation) / ROTATE_ANGLE_STEP) * ROTATE_ANGLE_STEP * Math.sign(rotation));

        var delta = (rotation - this.character.model.rotation.y) % (Math.PI * 2);

        if (delta > Math.PI) delta -= (Math.PI * 2);
        else
        if (delta < -Math.PI) delta = (Math.PI * 2) + delta;

        this.rotation.y = this.character.model.rotation.y + delta;

    },

    rotate: function(dt) {

        this.character.model.rotation.y = lerp(this.character.model.rotation.y, this.rotation.y, dt * 10);

    },

    rotateWeapon: function(rotation) {

        this.character.weapon.rotation.y = rotation;

    },

    move: function(offset) {

        this.character.position.add(offset);

    },

    destroy: function() {
        VG.EventDispatcher.unbind("Player-" + this.id + "-move.start", this.moveStart);
        VG.EventDispatcher.unbind("Player-" + this.id + "-move.end", this.moveEnd);
        VG.EventDispatcher.unbind("Player-" + this.id + "-rotation.set", this.rotationSet);
    }

};

export {Player};
