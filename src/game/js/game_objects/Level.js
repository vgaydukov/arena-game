import {BaseLevel} from "../base/BaseLevel";
import {Player} from "./Player";

function Level(data) {
    BaseLevel.apply(this, arguments);
    var context = this;

    this.players = [];

}

Level.prototype = Object.create(BaseLevel.prototype);
Level.constructor = Level;

Level.prototype.add = function(object) {
    VG.SceneEntity.prototype.add.apply(this, arguments);

    if (object instanceof Player) this.players.push(object);

};

export {Level};
