import {Weapon} from "./Weapon";
import {TANKS} from "../settings";

function Tank(data) {

    VG.SceneEntity.call(this);

    var asset = VG.EventDispatcher.query("AssetsLoader.getAsset", TANKS[data.tank].model);
    this.model = asset.clone();
    this.model.scale.copy(TANKS[data.tank].scale);
    this.model.position.copy(TANKS[data.tank].offset);
    this.model.castShadow = true;
    this.model.receiveShadow = true;
    this.add(this.model);

    this.weapon = new Weapon(data.weapon);
    this.weapon.position.copy(TANKS[data.tank].weaponPoint);
    this.add(this.weapon);

}

Tank.prototype = Object.create(VG.SceneEntity.prototype);
Tank.constructor = Tank;

export {Tank};
