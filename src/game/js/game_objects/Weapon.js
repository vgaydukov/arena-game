import {WEAPONS} from "../settings";

function Weapon(name) {

    VG.SceneEntity.call(this);

    var asset = VG.EventDispatcher.query("AssetsLoader.getAsset", WEAPONS[name].model);

    // var assetg = new THREE.BoxGeometry(1, 1, 7);
    // var assetm = new THREE.MeshBasicMaterial({color: "blue"});
    // var asset = new THREE.Mesh(assetg, assetm);
    // asset.position.copy(new THREE.Vector3(0, 0, 2));

    this.model = asset.clone();
    this.model.scale.copy(WEAPONS[name].scale);
    this.model.position.copy(WEAPONS[name].offset);
    this.model.castShadow = true;
    this.model.receiveShadow = true;
    this.add(this.model);

}

Weapon.prototype = Object.create(VG.SceneEntity.prototype);
Weapon.constructor = Weapon;

export {Weapon};
