import {CAMERA_OFFSET, CHAR_ANGLE_STEP} from "../settings";

function PlayerController() {

    VG.BaseEntity.call(this);

    this.target = null;

    this.mousePosition = new THREE.Vector2();
    this.cameraAngle = Math.atan2(CAMERA_OFFSET.x, CAMERA_OFFSET.z);

    this.moveKeyState = {
        up: 0,
        down: 0,
        right: 0,
        left: 0
    };

    this.moveDir = new THREE.Vector3(0, 0, 0);

}

PlayerController.prototype = Object.create(VG.BaseEntity.prototype);
PlayerController.constructor = PlayerController;

PlayerController.prototype.activate = function() {

    VG.BaseEntity.prototype.activate.apply(this, arguments);

    VG.EventDispatcher.bind("mouse.move", this, this.weaponRotation);
    VG.EventDispatcher.bind("mouse.down", this, this.attackStart);
    VG.EventDispatcher.bind("mouse.up", this, this.attackEnd);

    VG.EventDispatcher.bind("keyboard.keydown.w", this, this.moveUp);
    VG.EventDispatcher.bind("keyboard.keydown.a", this, this.moveLeft);
    VG.EventDispatcher.bind("keyboard.keydown.s", this, this.moveDown);
    VG.EventDispatcher.bind("keyboard.keydown.d", this, this.moveRight);

    VG.EventDispatcher.bind("keyboard.keyup.w", this, this.stopUp);
    VG.EventDispatcher.bind("keyboard.keyup.a", this, this.stopLeft);
    VG.EventDispatcher.bind("keyboard.keyup.s", this, this.stopDown);
    VG.EventDispatcher.bind("keyboard.keyup.d", this, this.stopRight);

};

PlayerController.prototype.deactivate = function() {

    VG.BaseEntity.prototype.deactivate.apply(this, arguments);

    VG.EventDispatcher.unbind("mouse.move", this.weaponRotation);
    VG.EventDispatcher.unbind("mouse.down", this.attackStart);
    VG.EventDispatcher.unbind("mouse.up", this.attackEnd);

    VG.EventDispatcher.unbind("keyboard.keydown.w", this.moveUp);
    VG.EventDispatcher.unbind("keyboard.keydown.a", this.moveLeft);
    VG.EventDispatcher.unbind("keyboard.keydown.s", this.moveDown);
    VG.EventDispatcher.unbind("keyboard.keydown.d", this.moveRight);

    VG.EventDispatcher.unbind("keyboard.keyup.w", this.stopUp);
    VG.EventDispatcher.unbind("keyboard.keyup.a", this.stopLeft);
    VG.EventDispatcher.unbind("keyboard.keyup.s", this.stopDown);
    VG.EventDispatcher.unbind("keyboard.keyup.d", this.stopRight);

};

PlayerController.prototype.setTarget = function(player) {

    this.target = player;
};

PlayerController.prototype.moveUp = function(evt) {
    this.moveKeyState.up = -1;
    this.move();
};

PlayerController.prototype.moveDown = function(evt) {
    this.moveKeyState.down = 1;
    this.move();
};

PlayerController.prototype.moveLeft = function(evt) {
    this.moveKeyState.left = -1;
    this.move();
};

PlayerController.prototype.moveRight = function(evt) {
    this.moveKeyState.right = 1;
    this.move();
};

PlayerController.prototype.stopUp = function(evt) {
    this.moveKeyState.up = 0;
    this.move();
};

PlayerController.prototype.stopDown = function(evt) {
    this.moveKeyState.down = 0;
    this.move();
};

PlayerController.prototype.stopLeft = function(evt) {
    this.moveKeyState.left = 0;
    this.move();
};

PlayerController.prototype.stopRight = function(evt) {
    this.moveKeyState.right = 0;
    this.move();
};

PlayerController.prototype.move = function() {
    
    this.moveDir.x = this.moveKeyState.left + this.moveKeyState.right;
    this.moveDir.z = this.moveKeyState.up + this.moveKeyState.down;

    var angle = this.cameraAngle + Math.atan2(this.moveDir.x, this.moveDir.z);

    if (this.moveDir.length() > 0) {
        this.target.moveStart();
    }
    else {
        this.target.moveEnd();
    }

    this.target.rotationSet(angle);
};

PlayerController.prototype.attackStart = function(evt) {
    if (!this.target) return;
};

PlayerController.prototype.attackEnd = function(evt) {
    if (!this.target) return;
};

PlayerController.prototype.weaponRotation = function(evt) {

    if (!this.target) return;

    this.screenMousePositionUpdate(evt);

    var mouseAngle = (Math.atan2(this.mousePosition.x, this.mousePosition.y));

    mouseAngle = (Math.sign(mouseAngle) || -1) * (Math.PI - Math.abs(mouseAngle)) + this.cameraAngle;
    // mouseAngle = (mouseAngle < 0 ? (2 * Math.PI) + mouseAngle : mouseAngle);

    this.target.rotateWeapon(mouseAngle);

};

PlayerController.prototype.screenMousePositionUpdate = function(evt) {

    var renderer = VG.EventDispatcher.query("Engine.get.renderer");

    var delta = 0;

    return function(evt) {
        renderer = renderer || VG.EventDispatcher.query("Engine.get.renderer");

        delta = renderer.domElement.clientWidth / renderer.domElement.clientHeight;

        this.mousePosition.x = ((evt.x / renderer.domElement.clientWidth) * 2 - 1);
        this.mousePosition.y = (-(evt.y / renderer.domElement.clientHeight) * 2 + 1);

    };
}();

export {PlayerController};
