var DEFAULT_TERRAIN_SCALE = [1, 1, 1];
var DEFAULT_TERRAIN_ROTATION = [0, 0, 0];

import {DEFAULT_IMAGE} from "../settings";

function BaseTerrain(params) {
    VG.SceneEntity.apply(this, arguments);

    this.autoUpdate = false;

    this.view.instance = this;

    var maxHeight = params.mxh;
    var minHeight = params.mnh;
    var blendVal = maxHeight - minHeight;

    var i1 = VG.EventDispatcher.query("AssetsLoader.getAsset", params.t.h.n) || DEFAULT_IMAGE;
    var i2 = VG.EventDispatcher.query("AssetsLoader.getAsset", params.t.t1.n) || DEFAULT_IMAGE;
    var i3 = VG.EventDispatcher.query("AssetsLoader.getAsset", params.t.t2.n) || DEFAULT_IMAGE;
    var i4 = VG.EventDispatcher.query("AssetsLoader.getAsset", params.t.t3.n) || DEFAULT_IMAGE;
    var i5 = VG.EventDispatcher.query("AssetsLoader.getAsset", params.t.t4.n) || DEFAULT_IMAGE;

    var t1 = new THREE.Texture(i2);
    t1.repeat.set(params.t.t1.r, params.t.t1.r);
    var t2 = new THREE.Texture(i3);
    t2.repeat.set(params.t.t2.r, params.t.t2.r);
    var t3 = new THREE.Texture(i4);
    t3.repeat.set(params.t.t3.r, params.t.t3.r);
    var t4 = new THREE.Texture(i5);
    t4.repeat.set(params.t.t4.r, params.t.t4.r);

    var material = VG.Terrain.generateBlendedMaterial([{
        texture: t1
    }, {
        texture: t2,
        levels: [blendVal * 0.05, blendVal * 0.3, blendVal * 0.7, blendVal]
    }, {
        texture: t3,
        glsl: "slope > 0.7853981633974483 ? 0.2 : 1.0 - smoothstep(0.47123889803846897, 0.7853981633974483, slope) + 0.2"
    }, {
        texture: t4,
        glsl: "1.0 - smoothstep(" + VG.Terrain.glslifyNumber(blendVal * 0.3) + " + smoothstep(-256.0, 256.0, vPosition.x) * 10.0, " + VG.Terrain.glslifyNumber(blendVal * 0.7) + ", vPosition.z)"
    }]);

    var xS = 128,
        yS = 128,
        /* eslint-disable new-cap */
        terrainScene = VG.Terrain({
        /* eslint-enable new-cap */
            easing: VG.Terrain.Linear,
            heightmap: i1,
            material: material,
            maxHeight: maxHeight,
            minHeight: minHeight,
            useBufferGeometry: false,
            xSegments: xS,
            xSize: params.xs,
            ySegments: yS,
            ySize: params.ys
        });
    terrainScene.children[0].castShadow = true;
    terrainScene.children[0].receiveShadow = true;
    this.view.position.fromArray(params.pos);
    this.view.scale.fromArray(params.scl || DEFAULT_TERRAIN_SCALE);
    this.view.rotation.fromArray(params.rot || DEFAULT_TERRAIN_ROTATION);
    this.add(terrainScene);
}

BaseTerrain.prototype = Object.create(VG.SceneEntity.prototype);
BaseTerrain.constructor = BaseTerrain;

export {BaseTerrain};
