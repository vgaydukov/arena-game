import {BaseTerrain} from "./BaseTerrain";

function BaseLevel(data) {
    VG.SceneEntity.call(this);
    this.matrixAutoUpdate = false;

    var context = this;

    let assets = data.assets;
    data = data.level;

    this.size = data.size;
    this.step = data.step;
    this.realSize = data.realSize;

    this.alight = new THREE.AmbientLight(data.alc);
    this.add(this.alight);

    this.dlight1 = new THREE.DirectionalLight(data.dl1c, data.dl1i);
    this.dlight1.position.fromArray(data.dl1p);
    this.dlight1.castShadow = data.dl1sh;
    if (this.dlight1.castShadow) {
        this.dlight1.shadow.camera.left = -data.dl1shsize;
        this.dlight1.shadow.camera.right = data.dl1shsize;
        this.dlight1.shadow.camera.top = data.dl1shsize;
        this.dlight1.shadow.camera.bottom = -data.dl1shsize;
        this.dlight1.shadow.mapSize.width = data.dl1shq;
        this.dlight1.shadow.mapSize.height = data.dl1shq;
        this.dlight1.shadow.camera.near = data.dl1shnear;
        this.dlight1.shadow.camera.far = data.dl1shfar;
        this.dlight1.shadow.bias = data.dl1shbias;
    }
    this.add(this.dlight1);

    this.dlight2 = new THREE.DirectionalLight(data.dl2c, data.dl2i);
    this.dlight2.position.fromArray(data.dl2p);
    this.add(this.dlight2);

    this.collisionPlane = new THREE.Mesh(
        new THREE.PlaneGeometry(1, 1),
        new THREE.MeshBasicMaterial({opacity: 0, transparent: true})
    );
    this.collisionPlane.rotation.x = -Math.PI / 2;
    this.collisionPlane.scale.set(this.size * 2, this.size * 2, this.size * 2);
    this.add(this.collisionPlane);

    this.matrix = new VG.Matrix2D((this.size / this.step), (this.size / this.step), data.matrix);

    this.fillLevel(data.assets);

    if (data.terrain) this.createTerrain(data.terrain);

}

BaseLevel.prototype = Object.create(VG.SceneEntity.prototype);
BaseLevel.constructor = BaseLevel;

BaseLevel.prototype.fillLevel = function(assets) {
    for (var a in assets) {
        var asset = VG.EventDispatcher.query("AssetsLoader.getAsset", a);

        for (var i = assets[a].length - 1; i >= 0; i--) {
            var clone = asset.clone();
            clone.position.fromArray(assets[a][i].p);
            clone.rotation.fromArray(assets[a][i].r);
            clone.scale.fromArray(assets[a][i].s);
            clone.needCalcCells = assets[a][i].c;
            clone.castShadow = assets[a][i].shc;
            clone.receiveShadow = assets[a][i].shr;
            this.add(clone);
        }
    }
};

BaseLevel.prototype.createTerrain = function(params) {

    var terrain = new BaseTerrain(params);
    this.add(terrain);

};

export {BaseLevel};
