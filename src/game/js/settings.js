export const STATIC_PATH = "/static/";
export const LEVELS_PATH = "levels/";
export const PACK_PATH = "packs/";
export const DOM_ELEMENT = document.getElementById("main");
export const CAMERA_OFFSET = new THREE.Vector3(40, 70, 40);
export const CHAR_STATE = {
    STAND: 0,
    MOVE: 1,
    ATTACK: 2
};
export const ROTATE_ANGLE_STEP = Math.PI / 4;
export const TANK_SPEED = 20;

export const DEFAULT_IMAGE = (function() {
    var canvas = document.createElement("canvas");
    canvas.width = 128;
    canvas.height = 128;
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, 128, 128);

    var img = document.createElement("img");
    img.src = canvas.toDataURL("image/png");
    return img;
})();

export const TANKS = {
    "pioneer": {
        "name": "Pioneer",
        "description": "the first of all",
        "model": "tank1",
        "offset": new THREE.Vector3(0, 3, 0),
        "weaponPoint": new THREE.Vector3(0, 5, 0),
        "scale": new THREE.Vector3(0.03, 0.03, 0.03)
    }
};

export const WEAPONS = {
    "gun": {
        "name": "Gun",
        "description": "the first of all",
        "model": "gun1",
        "offset": new THREE.Vector3(0, 0, 0),
        "scale": new THREE.Vector3(0.02, 0.02, 0.02)
    }
};
