import {BaseLevel} from "./../../js/base/BaseLevel";
import {Terrain} from "./Terrain";

function Level(data) {

    BaseLevel.apply(this, arguments);

    this.autoUpdate = false;
    this.name = data.name || "";

    this.lightHelper = new THREE.CameraHelper(this.dlight1.shadow.camera);
    this.lightHelper.ignoreRaycaster = true;
    this.add(this.lightHelper);

    VG.EventDispatcher.bind("Level.getData", this, this.getData);
    VG.EventDispatcher.bind("Level.addAsset", this, this.addAsset);
    VG.EventDispatcher.bind("Level.createTerrain", this, this.createTerrain);
    VG.EventDispatcher.bind("Level.removeAsset", this, this.removeAsset);
    VG.EventDispatcher.bind("Level.updateParams", this, this.updateParams);
    VG.EventDispatcher.bind("Level.clear", this, this.clear);

    VG.EventDispatcher.send("Level.updated", this.getData().level);
}

Level.prototype = Object.create(BaseLevel.prototype);
Level.constructor = Level;

Level.prototype.assets = [];

Level.prototype.fillLevel = function(assets) {
    for (var a in assets) {
        var asset = VG.EventDispatcher.query("AssetsPallete.getAsset", a);

        for (var i = assets[a].length - 1; i >= 0; i--) {
            var clone = asset.clone();
            clone.position.fromArray(assets[a][i].p);
            clone.rotation.fromArray(assets[a][i].r);
            clone.scale.fromArray(assets[a][i].s);
            clone.needCalcCells = assets[a][i].c;
            this.addAsset(clone);
        }
    }
};

Level.prototype.createTerrain = function(params) {
    var position = new THREE.Vector3();

    var terrain = new Terrain(params);

    if (this.terrain) {
        position = this.terrain.view.position;
        this.remove(this.terrain);
        terrain.position.copy(position);
    }

    this.terrain = terrain;
    this.add(this.terrain);
};

Level.prototype.getData = function() {

    var data = {};
    data.level = {};
    data.level.name = this.name;
    data.level.size = this.size;
    data.level.step = this.step;
    data.level.alc = this.alight.color.getHex();
    data.level.ali = this.alight.intensity;

    data.level.dl1c = this.dlight1.color.getHex();
    data.level.dl1i = this.dlight1.intensity;
    data.level.dl1p = this.dlight1.position.toArray();
    data.level.dl1sh = this.dlight1.castShadow;
    data.level.dl1shsize = this.dlight1.shadow.camera.right;
    data.level.dl1shq = this.dlight1.shadow.mapSize.width;
    data.level.dl1shnear = this.dlight1.shadow.camera.near;
    data.level.dl1shfar = this.dlight1.shadow.camera.far;
    data.level.dl1shbias = this.dlight1.shadow.bias;

    data.level.dl2c = this.dlight2.color.getHex();
    data.level.dl2i = this.dlight2.intensity;
    data.level.dl2p = this.dlight2.position.toArray();

    data.assets = [];

    if (this.terrain) {
        data.level.terrain = this.terrain.getData();

        for (var i in data.level.terrain.t) {
            var image = data.level.terrain.t[i].n;
            var a = VG.EventDispatcher.query("AssetsPallete.getAsset", image);

            if (data.assets.indexOf(a.assetUrl) < 0) data.assets.push(a.assetUrl);
        }
    }

    data.level.assets = {};

    for (i = this.assets.length - 1; i >= 0; i--) {
        var asset = this.assets[i];
        a = VG.EventDispatcher.query("AssetsPallete.getAsset", asset.name);

        if (data.assets.indexOf(a.assetUrl) < 0) data.assets.push(a.assetUrl);

        if (!data.level.assets[asset.name]) data.level.assets[asset.name] = [];

        data.level.assets[asset.name].push({
            p: asset.position.toArray(),
            s: asset.scale.toArray(),
            r: asset.rotation.toArray(),
            c: asset.needCalcCells,
            shc: asset.mesh.castShadow,
            shr: asset.mesh.receiveShadow
        });
    }

    if (this.matrix) data.level.matrix = Array.prototype.slice.call(this.matrix.matrix);

    return data;
};

Level.prototype.updateParams = function(data) {

    data = data || {};

    this.name = data.name;

    if (data.size != this.size || data.step != this.step) {
        this.size = data.size || this.size;
        this.step = data.step || this.step;
    }

    var offset = this.step / 2;

    this.collisionPlane.scale.set(this.size, this.size, this.size);
    this.collisionPlane.position.x = -offset;
    this.collisionPlane.position.z = -offset;

    this.updateLights(data);

    VG.EventDispatcher.send("Level.updated", this.getData().level);

};

Level.prototype.updateLights = function(data) {
    !data.alc || this.alight.color.set(data.alc);
    !data.ali || (this.alight.intensity = data.ali);

    !data.dl1c || this.dlight1.color.set(data.dl1c);
    !data.dl1i || (this.dlight1.intensity = data.dl1i);
    !data.dl1p || this.dlight1.position.fromArray(data.dl1p);

    !data.dl2c || this.dlight2.color.set(data.dl2c);
    !data.dl2i || (this.dlight2.intensity = data.dl2i);
    !data.dl2p || this.dlight2.position.fromArray(data.dl2p);

    if (data.dl1sh) {
        this.dlight1.castShadow = data.dl1sh;
        this.dlight1.shadow.camera.left = -data.dl1shsize;
        this.dlight1.shadow.camera.right = data.dl1shsize;
        this.dlight1.shadow.camera.top = data.dl1shsize;
        this.dlight1.shadow.camera.bottom = -data.dl1shsize;
        this.dlight1.shadow.mapSize.width = data.dl1shq;
        this.dlight1.shadow.mapSize.height = data.dl1shq;
        this.dlight1.shadow.camera.near = data.dl1shnear;
        this.dlight1.shadow.camera.far = data.dl1shfar;
        this.dlight1.shadow.bias = data.dl1shbias;
        this.dlight1.shadow.camera.updateProjectionMatrix();
    }
    else {
        this.dlight1.castShadow = false;
    }

    this.lightHelper.update();
};

Level.prototype.addAsset = function(asset) {

    this.add(asset);
    this.assets.push(asset);

};

Level.prototype.removeAsset = function(asset) {

    this.remove(asset);
    this.assets.splice(this.assets.indexOf(asset), 1);

};

Level.prototype.loadLocalSave = function() {
    var data = localStorage.getItem("level");
    if (!data) return;

    var pako = require("pako"); // eslint-disable-line global-require
    data = atob(data);
    data = pako.inflate(data, {
        to: "string"
    });
    data = JSON.parse(data);
    this.createFromData(data);

};

Level.prototype.clear = function() {
    localStorage.removeItem("level");
    for (var i = this.assets.length - 1; i >= 0; i--) {
        this.remove(this.assets[i]);
    }
    this.assets = [];
    this.matrix = new VG.Matrix2D((this.size / this.step), (this.size / this.step));

};

export {Level};
