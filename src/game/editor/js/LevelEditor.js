import {SceneEditor} from "./scenes/SceneEditor";
import {SceneLoading} from "./scenes/SceneLoading";
import {LevelSelectMenuUI} from "./ui/LevelSelectMenuUI";
import {STATIC_PATH} from "./../../js/settings";

function LevelEditor(domId) {

    var context = this;

    VG.SETTINGS.CLEAR_COLOR = "white";
    var engine = new VG.Engine(domId);

    this.sceneController = new VG.SceneController();
    engine.add(this.sceneController);

    var editorScene = new SceneEditor();
    this.sceneController.add(editorScene);

    var loadingScene = new SceneLoading();
    this.sceneController.add(loadingScene);

    var renderer = VG.EventDispatcher.query("Engine.get.renderer");
    renderer.shadowMap.enabled = true;

    var keyboard = new VG.KeyboardEventsHandler(document);
    var mouse = new VG.MouseEventsHandler(renderer.domElement);

    this.levelSelector = new LevelSelectMenuUI();

    this.assetsLoader = new VG.AssetsLoader(STATIC_PATH + "assets/");

    VG.EventDispatcher.bind("LevelEditor.newLevel", this, this.loadLevel);
    VG.EventDispatcher.bind("LevelEditor.loadLevel", this, this.loadLevel);
    VG.EventDispatcher.bind("LevelEditor.showLevelMenu", this, this.showLevelMenu);

    this.showLevelMenu();

}

LevelEditor.prototype = {
    constructor: VG.LevelEditor,

    request: function(url, callback) {
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var data = JSON.parse(this.responseText);
                callback(data);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    },

    newLevel: function(name) {
        var context = this;

        this.sceneController.activateScene("loading");
        this.sceneController.activateScene("editor", data);
    },

    loadLevel: function(name) {
        var context = this;

        this.sceneController.activateScene("loading");

        this.assetsLoader.loadPack(STATIC_PATH + "assets/" + name,
            function() {

            },
            function(data) {
                context.sceneController.activeScene.update(data);
            },
            function(data) {
                VG.EventDispatcher.send("LevelEditor.loaded", data);
                context.sceneController.activateScene("editor", data);
            });
    },
    showLevelMenu: function(data) {

        var context = this;

        this.request("/levels", function(levels) {

            context.assetsLoader.loadPack("/assets/bundle",
                function() {
                    context.sceneController.activateScene("loading");
                },
                function(data) {
                    context.sceneController.activeScene.update(data);
                },
                function(data) {
                    context.levelSelector.show(levels);
                });
        });
    }
};

export {LevelEditor};
