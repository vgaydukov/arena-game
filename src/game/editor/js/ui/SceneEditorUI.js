import {AssetsPalleteUI} from "./AssetsPalleteUI";
import {TerrainSettingsUI} from "./TerrainSettingsUI";

function SceneEditorUI(container) {
    VG.UI.Container.apply(this, arguments);

    var context = this;

    VG.EventDispatcher.bind("Editor.meshSelected", this, this.meshSelected);
    VG.EventDispatcher.bind("Editor.meshUnSelected", this, this.hideAssetMenu);
    VG.EventDispatcher.bind("Level.updated", this, this.levelUpdated);

    var pallete = new AssetsPalleteUI();
    var terrainSettings = new TerrainSettingsUI();

    this.panel = new dat.GUI({width: 310});
    this.panel.domElement.style.display = "none";

    this.folderEditor = this.panel.addFolder("Editor");
    this.folderLevel = this.panel.addFolder("Level");
    this.folderTerrain = this.panel.addFolder("Terrain");
    this.folderSelectedMesh = this.panel.addFolder("Selected mesh");

    this.settings = {
        "Open": function() {
            VG.EventDispatcher.send("LevelEditor.showLevelMenu");
        },
        "Name": "",

        "Save": function() {
            VG.EventDispatcher.send("Editor.saveFile");
        },
        "Clear": function() {
            VG.EventDispatcher.send("Level.clear");
        },
        "Open pallete": function() {
            pallete.show();
        },
        "Cursor step": 1,
        "Recalculate matrix": function() {
            VG.EventDispatcher.send("MatrixTool.reCalculateMatrix");
        },

        "Activate mode": "assetsMode",

        "Size": 0,
        "Step": 0,

        "AL color": 16777215,
        "AL intencity": 1,

        "DL1 color": 16777215,
        "DL1 intencity": 1,
        "DL1 position x": 0,
        "DL1 position y": 0,
        "DL1 position z": 0,
        "DL1 cast shadow": true,
        "DL1 shadow size" : 0,
        "DL1 shadow quality" : 0,
        "DL1 shadow near": 0,
        "DL1 shadow far": 0,
        "DL1 shadow bias": 0.0001,
        "DL2 color": 16777215,
        "DL2 intencity": 1,
        "DL2 position x": 0,
        "DL2 position y": 0,
        "DL2 position z": 0,
        "Open terrain settings": function() {
            terrainSettings.show();
        },
        "Generate terrain": function() {
            VG.EventDispatcher.send("Level.createTerrain", terrainSettings.getData());
        },

        "Position X": 0,
        "Position Y": 0,
        "Position Z": 0,
        "Rotation X": 0,
        "Rotation Y": 0,
        "Rotation Z": 0,
        "Calculate cells": true,

        "Scale": 0
    };

    this.controllers = {};

    this.folderEditor.add(this.settings, "Open");
    this.folderEditor.add(this.settings, "Name").onChange(function() {
        context.updateLevel();
    });
    this.folderEditor.add(this.settings, "Save");
    this.folderEditor.add(this.settings, "Clear");
    this.folderEditor.add(this.settings, "Open pallete");
    this.controllers["Cursor step"] = this.folderEditor.add(this.settings, "Cursor step", 0, 1000, 0.1).onChange(function() {
        context.updateEditor();
    });
    this.folderEditor.add(this.settings, "Activate mode", ["assetsMode", "cellsMode"]).onChange(function(name) {
        VG.EventDispatcher.send("Editor.activateMode", name);
    });

    this.folderEditor.open();

    this.folderLevel.add(this.settings, "Recalculate matrix");

    this.controllers["Size"] = this.folderLevel.add(this.settings, "Size", 100, 10000, 10).onChange(function() {
        context.updateLevel();
    });
    this.controllers["Step"] = this.folderLevel.add(this.settings, "Step", 1, 1000, 1).onChange(function() {
        context.updateLevel();
    });

    this.folderLevel.addColor(this.settings, "AL color").onChange(function() {
        context.updateLevel();
    });
    this.folderLevel.add(this.settings, "AL intencity", 0, 10, 0.1).onChange(function() {
        context.updateLevel();
    });

    this.folderLevel.addColor(this.settings, "DL1 color").onChange(function() {
        context.updateLevel();
    });

    this.folderLevel.add(this.settings, "DL1 intencity", 0, 10, 0.1).onChange(function() {
        context.updateLevel();
    });
    this.controllers["DL1 position x"] = this.folderLevel.add(this.settings, "DL1 position x", -2000, 2000, 0.00001).onChange(function() {
        context.updateLevel();
    });
    this.controllers["DL1 position y"] = this.folderLevel.add(this.settings, "DL1 position y", -2000, 2000, 0.00001).onChange(function() {
        context.updateLevel();
    });
    this.controllers["DL1 position z"] = this.folderLevel.add(this.settings, "DL1 position z", -2000, 2000, 0.00001).onChange(function() {
        context.updateLevel();
    });
    this.folderLevel.add(this.settings, "DL1 cast shadow", this.settings["DL1 cast shadow"]).onChange(function() {
        context.updateLevel();
    });
    this.controllers["DL1 shadow size"] = this.folderLevel.add(this.settings, "DL1 shadow size", 0, 2000, 1).onChange(function() {
        context.updateLevel();
    });

    this.folderLevel.add(this.settings, "DL1 shadow quality", 128, 2048, 128).onChange(function() {
        context.updateLevel();
    });

    this.folderLevel.add(this.settings, "DL1 shadow near", 0, 2000, 1).onChange(function() {
        context.updateLevel();
    });

    this.folderLevel.add(this.settings, "DL1 shadow far", 0, 2000, 1).onChange(function() {
        context.updateLevel();
    });
    this.folderLevel.add(this.settings, "DL1 shadow bias", -1, 1, 0.00001).onChange(function() {
        context.updateLevel();
    });

    this.folderLevel.addColor(this.settings, "DL2 color").onChange(function() {
        context.updateLevel();
    });

    this.folderLevel.add(this.settings, "DL2 intencity", 0, 10, 0.1).onChange(function() {
        context.updateLevel();
    });
    this.controllers["DL2 position x"] = this.folderLevel.add(this.settings, "DL2 position x", -2000, 2000, 0.00001).onChange(function() {
        context.updateLevel();
    });
    this.controllers["DL2 position y"] = this.folderLevel.add(this.settings, "DL2 position y", -2000, 2000, 0.00001).onChange(function() {
        context.updateLevel();
    });
    this.controllers["DL2 position z"] = this.folderLevel.add(this.settings, "DL2 position z", -2000, 2000, 0.00001).onChange(function() {
        context.updateLevel();
    });

    this.folderTerrain.add(this.settings, "Open terrain settings");
    this.folderTerrain.add(this.settings, "Generate terrain");

    this.controllers["Position X"] = this.folderSelectedMesh.add(this.settings, "Position X", -2000, 2000, 0.00001).onChange(function() {
        context.updateMesh();
    });
    this.controllers["Position Y"] = this.folderSelectedMesh.add(this.settings, "Position Y", -2000, 2000, 0.00001).onChange(function() {
        context.updateMesh();
    });
    this.controllers["Position Z"] = this.folderSelectedMesh.add(this.settings, "Position Z", -2000, 2000, 0.00001).onChange(function() {
        context.updateMesh();
    });

    this.folderSelectedMesh.add(this.settings, "Rotation X", 0, Math.PI * 2, Math.PI / 10).onChange(function() {
        context.updateMesh();
    });
    this.folderSelectedMesh.add(this.settings, "Rotation Y", 0, Math.PI * 2, Math.PI / 10).onChange(function() {
        context.updateMesh();
    });
    this.folderSelectedMesh.add(this.settings, "Rotation Z", 0, Math.PI * 2, Math.PI / 10).onChange(function() {
        context.updateMesh();
    });
    this.folderSelectedMesh.add(this.settings, "Scale", 0.001, 1000, 0.001).onChange(function() {
        context.updateMesh();
    });
    this.folderSelectedMesh.add(this.settings, "Calculate cells").onChange(function() {
        context.updateMesh();
    });
    this.folderSelectedMesh.open();
    this.hideAssetMenu();

}

SceneEditorUI.prototype = Object.create(VG.UI.Container.prototype);
SceneEditorUI.constructor = SceneEditorUI;

SceneEditorUI.prototype.show = function() {
    VG.UI.Container.prototype.show.call(this);
    this.panel.domElement.style.display = "";
};
SceneEditorUI.prototype.hide = function() {
    VG.UI.Container.prototype.hide.call(this);
    this.panel.domElement.style.display = "none";
};

SceneEditorUI.prototype.updateEditor = function() {

    var context = this;

    var data = {
        step: context.settings["Cursor step"]
    };

    VG.EventDispatcher.send("Editor.updateParams", data);
};

SceneEditorUI.prototype.updateLevel = function() {

    var context = this;

    while ((context.settings["Size"] / context.settings["Step"]) % 10 > 0) {
        context.settings["Step"]++;
        if (context.settings["Step"] > context.settings["Size"] * 0.1) context.settings["Step"] = 1;
    }

    context.settings["Step"] = context.settings["Step"] / context.settings["Size"] > 0.1 ?
        context.settings["Size"] * 0.1 :
        context.settings["Step"];

    var data = {
        name: context.settings["Name"],
        size: context.settings["Size"],
        step: context.settings["Step"],
        alc: context.settings["AL color"],
        ali: context.settings["AL intencity"],
        dl1c: context.settings["DL1 color"],
        dl1i: context.settings["DL1 intencity"],
        dl1p: [context.settings["DL1 position x"], context.settings["DL1 position y"], context.settings["DL1 position z"]],
        dl1sh: context.settings["DL1 cast shadow"],
        dl1shsize: context.settings["DL1 shadow size"],
        dl1shq: context.settings["DL1 shadow quality"],
        dl1shnear: context.settings["DL1 shadow near"],
        dl1shfar: context.settings["DL1 shadow far"],
        dl1shbias: context.settings["DL1 shadow bias"],
        dl2c: context.settings["DL2 color"],
        dl2i: context.settings["DL2 intencity"],
        dl2p: [context.settings["DL2 position x"], context.settings["DL2 position y"], context.settings["DL2 position z"]]

    };
    this.folderLevel.updateDisplay();

    VG.EventDispatcher.send("Level.updateParams", data);
};

SceneEditorUI.prototype.levelUpdated = function(levelData) {

    var halfSize = levelData.size / 2;

    this.controllers["DL1 position x"].min(-halfSize).max(halfSize);
    this.controllers["DL1 position y"].min(levelData.size * 0.1).max(levelData.size);
    this.controllers["DL1 position z"].min(-halfSize).max(halfSize);
    this.controllers["DL2 position x"].min(-halfSize).max(halfSize);
    this.controllers["DL2 position y"].min(-halfSize).max(halfSize);
    this.controllers["DL2 position z"].min(-halfSize).max(halfSize);
    this.controllers["DL1 shadow size"].max(levelData.size);
    this.controllers["Position X"].min(-halfSize).max(halfSize);
    this.controllers["Position Y"].min(-halfSize).max(halfSize);
    this.controllers["Position Z"].min(-halfSize).max(halfSize);

    this.settings["Name"] = levelData.name;

    this.settings["Size"] = levelData.size;
    this.settings["Step"] = levelData.step;

    this.settings["AL color"] = levelData.alc;
    this.settings["AL intencity"] = levelData.ali;

    this.settings["DL1 color"] = levelData.dl1c;
    this.settings["DL1 intencity"] = levelData.dl1i;
    this.settings["DL1 position x"] = levelData.dl1p[0];
    this.settings["DL1 position y"] = levelData.dl1p[1];
    this.settings["DL1 position z"] = levelData.dl1p[2];

    this.settings["DL1 cast shadow"] = levelData.dl1sh;
    this.settings["DL1 shadow size"] = levelData.dl1shsize;
    this.settings["DL1 shadow quality"] = levelData.dl1shq;
    this.settings["DL1 shadow near"] = levelData.dl1shnear;
    this.settings["DL1 shadow far"] = levelData.dl1shfar;
    this.settings["DL1 shadow bias"] = levelData.dl1shbias;

    this.settings["DL2 color"] = levelData.dl2c;
    this.settings["DL2 intencity"] = levelData.dl2i;
    this.settings["DL2 position x"] = levelData.dl2p[0];
    this.settings["DL2 position y"] = levelData.dl2p[1];
    this.settings["DL2 position z"] = levelData.dl2p[2];

    var assets = VG.EventDispatcher.query("AssetsPallete.getAssets");
    var images = [];
    for (var asset in assets) {
        if (assets[asset] instanceof Image) {
            images.push(asset);
        }
    }

    this.folderLevel.updateDisplay();
};

SceneEditorUI.prototype.meshSelected = function(asset) {

    this.folderSelectedMesh.domElement.style.display = "";
    this.folderLevel.close();

    this.settings["Scale"] = asset.scale.x;

    this.settings["Rotation X"] = asset.rotation.x;
    this.settings["Rotation Y"] = asset.rotation.y;
    this.settings["Rotation Z"] = asset.rotation.z;

    this.settings["Position X"] = asset.position.x;
    this.settings["Position Y"] = asset.position.y;
    this.settings["Position Z"] = asset.position.z;

    this.settings["Calculate cells"] = asset.needCalcCells;

    this.folderSelectedMesh.updateDisplay();
};

SceneEditorUI.prototype.updateMesh = function() {

    var context = this;

    var data = {
        rotation: new THREE.Euler(
            context.settings["Rotation X"],
            context.settings["Rotation Y"],
            context.settings["Rotation Z"]),
        position: new THREE.Vector3(
            context.settings["Position X"],
            context.settings["Position Y"],
            context.settings["Position Z"]),
        size: context.settings["Scale"],
        needCalcCells: context.settings["Calculate cells"]
    };

    VG.EventDispatcher.send("Editor.updateMesh", data);
};

SceneEditorUI.prototype.hideAssetMenu = function(asset) {
    this.folderSelectedMesh.domElement.style.display = "none";
};

export {SceneEditorUI};
