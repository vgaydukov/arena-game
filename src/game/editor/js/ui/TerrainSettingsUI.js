function TerrainSettingsUI(container) {
    VG.UI.Container.apply(this, arguments);

    $(this.view).dialog({
        title: "Terrain settings",
        resizable: false,
        width: 475,
        height: 400,
        autoOpen: false
    });

    this.terrainParams = {
        mnh: 0,
        mxh: 100,
        xs: 100,
        ys: 100,
        pos: [0, -55, 0],
        scl: [1, 1, 1],
        rot: [0, 0, 0],
        t: {
            "h": {
                "n": null
            },
            "t1": {
                "n": null,
                "r": 1
            },
            "t2": {
                "n": null,
                "r": 1
            },
            "t3": {
                "n": null,
                "r": 1
            },
            "t4": {
                "n": null,
                "r": 1
            }
        }
    };
}

TerrainSettingsUI.prototype = Object.create(VG.UI.Container.prototype);
TerrainSettingsUI.constructor = TerrainSettingsUI;

TerrainSettingsUI.prototype.createTextureOption = function(name, value, settings) {

    var context = this;

    var blockContainer = document.createElement("div");
    blockContainer.style.display = "inline-block";
    blockContainer.style.width = "50%";
    this.add(blockContainer);

    var header = document.createElement("p");
    $(header).text(name);
    blockContainer.appendChild(header);

    var textureselect = document.createElement("select");
    for (var i = 0; i < value.length; i++) {
        $(textureselect).append($("<option></option>").attr(value[i], value[i]).text(value[i]));
    }
    blockContainer.appendChild(textureselect);

    $(textureselect).selectmenu({
        width: "90%"
    }).on("selectmenuchange", function(val, ui) {
        settings["n"] = ui.item.value;
    });

    if (!settings["n"]) settings["n"] = value[0];
    else {
        $(textureselect).val(settings["n"]);
        $(textureselect).selectmenu("refresh");
    }

    if (!settings.hasOwnProperty("r")) return;

    var repeat = document.createElement("input");
    blockContainer.appendChild(repeat);

    $(repeat).spinner({
        min: 1
    }).width(155).on("spinchange", function(event, ui) {
        settings["r"] = parseInt(this.value);
    });
    $(repeat).val(settings["r"]);

};

TerrainSettingsUI.prototype.show = function() {
    VG.UI.Container.prototype.show.call(this);

    $(this.view).html("");

    this.terrainParams = VG.EventDispatcher.query("Level.getData").level.terrain || this.terrainParams;

    var context = this;

    $(context.view).dialog("open");

    var minHeightHeader = document.createElement("p");
    $(minHeightHeader).text("Min height");
    this.add(minHeightHeader);
    var minHeight = document.createElement("input");
    this.add(minHeight);

    $(minHeight).spinner({
        min: 0
    }).on("spinchange", function(event, ui) {
        context.terrainParams["mnh"] = parseInt(this.value);
    });
    $(minHeight).val(context.terrainParams["mnh"] || 0);

    var maxHeightHeader = document.createElement("p");
    $(maxHeightHeader).text("Max height");
    this.add(maxHeightHeader);
    var maxHeight = document.createElement("input");
    this.add(maxHeight);

    $(maxHeight).spinner({
        min: 100
    }).on("spinchange", function(event, ui) {
        context.terrainParams["mxh"] = parseInt(this.value);
    });
    $(maxHeight).val(context.terrainParams["mxh"] || 100);

    var xSizeHeader = document.createElement("p");
    $(xSizeHeader).text("X size");
    this.add(xSizeHeader);
    var xSize = document.createElement("input");
    this.add(xSize);

    $(xSize).spinner({
        min: 100
    }).on("spinchange", function(event, ui) {
        context.terrainParams["xs"] = parseInt(this.value);
    });
    $(xSize).val(context.terrainParams["xs"] || 100);

    var ySizeHeader = document.createElement("p");
    $(ySizeHeader).text("Y size");
    this.add(ySizeHeader);
    var ySize = document.createElement("input");
    this.add(ySize);

    $(ySize).spinner({
        min: 100
    }).on("spinchange", function(event, ui) {
        context.terrainParams["ys"] = parseInt(this.value);
    });
    $(ySize).val(context.terrainParams["ys"] || 100);

    var textureList = [];
    var assets = VG.EventDispatcher.query("AssetsPallete.getAssets");

    for (var a in assets) {
        if (assets[a] instanceof Image) textureList.push(a);
    }

    this.createTextureOption("Main texture", textureList, this.terrainParams.t.t1);
    this.createTextureOption("Texture 1", textureList, this.terrainParams.t.t2);
    this.createTextureOption("Texture 2", textureList, this.terrainParams.t.t3);
    this.createTextureOption("Texture 3", textureList, this.terrainParams.t.t4);
    this.createTextureOption("Heightmap", textureList, this.terrainParams.t.h);
};

TerrainSettingsUI.prototype.hide = function(data) {

    VG.UI.Container.prototype.hide.call(this);
    $(this.view).dialog("close");

};

TerrainSettingsUI.prototype.getData = function() {
    return this.terrainParams;
};

export {TerrainSettingsUI};
