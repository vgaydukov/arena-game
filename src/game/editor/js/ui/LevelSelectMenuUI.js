function LevelSelectMenuUI(container) {
    VG.UI.Container.apply(this, arguments);

    $(this.view).dialog({
        title: "Select level",
        resizable: false,
        width: 475,
        height: 400,
        autoOpen: false
    });

}

LevelSelectMenuUI.prototype = Object.create(VG.UI.Container.prototype);
LevelSelectMenuUI.constructor = LevelSelectMenuUI;

LevelSelectMenuUI.prototype.show = function(data) {

    VG.UI.Container.prototype.show.call(this);

    var context = this;

    while (this.view.firstChild) {
        this.view.removeChild(this.view.firstChild);
    }

    if (data) {
        var levelButton = document.createElement("div");
        levelButton.style.width = "100px";
        levelButton.style.margin = "2px";

        for (var i = 0; i < data.levels.length; i++) {

            var button = levelButton.cloneNode();
            button.levelName = data.levels[i];
            this.add(button);

            $(button).button({
                width: "100px",
                label: data.levels[i]
            }).click(function() {

                VG.EventDispatcher.send("LevelEditor.loadLevel", this.levelName);
                $(context.view).dialog("close");

            });
        }
    }

    $(context.view).dialog("open");

};

LevelSelectMenuUI.prototype.hide = function(data) {

    VG.UI.Container.prototype.hide.call(this);
    $(this.view).dialog("close");

};

export {LevelSelectMenuUI};
