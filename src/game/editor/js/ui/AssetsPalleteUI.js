function AssetsPalleteUI(container) {
    VG.UI.Container.apply(this, arguments);

    $(this.view).dialog({
        title: "Select asset",
        resizable: false,
        width: 475,
        height: 400,
        autoOpen: false
    });

    this.assetsLoaded = false;

}

AssetsPalleteUI.prototype = Object.create(VG.UI.Container.prototype);
AssetsPalleteUI.constructor = AssetsPalleteUI;

AssetsPalleteUI.prototype.show = function() {
    VG.UI.Container.prototype.show.call(this);

    var context = this;

    $(context.view).dialog("open");

    if (this.assetsLoaded) return;

    var data = VG.EventDispatcher.query("AssetsPalleteUI.getAssets");

    var assetButton = document.createElement("div");
    assetButton.style.width = "100px";
    assetButton.style.margin = "2px";

    for (var asset in data) {

        var button = assetButton.cloneNode();
        button.assetName = asset;
        this.add(button);

        if (data[asset] instanceof Image) {
            $(button).button({
                width: "100px",
                label: asset
            });

            let img = data[asset].cloneNode();
            img.style.width = "80px";
            img.style.height = "80px";
            $(button).append(img);
        }
        else {
            $(button).button({
                width: "100px",
                label: asset
            }).click(function() {

                VG.EventDispatcher.send("AssetsPalleteUI.selectAsset", this.assetName);
                $(context.view).dialog("close");

            });
        }

    }
    this.assetsLoaded = true;
};

AssetsPalleteUI.prototype.hide = function(data) {

    VG.UI.Container.prototype.hide.call(this);
    $(this.view).dialog("close");

};

export {AssetsPalleteUI};
