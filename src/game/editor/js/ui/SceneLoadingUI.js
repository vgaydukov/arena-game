function SceneLoadingUI(name) {

    VG.UI.Container.apply(this, arguments);

    var progress = document.createElement("div");
    progress.style.position = "absolute";
    progress.style.bottom = "20px";
    progress.style.width = "400px";
    progress.style.left = "calc(50% - 200px)";

    this.add(progress);

    this.update = function(val) {
        $(progress).progressbar({
            value: val * 100
        });
    };

}

SceneLoadingUI.prototype = Object.create(VG.UI.Container.prototype);
SceneLoadingUI.constructor = SceneLoadingUI;

SceneLoadingUI.prototype.show = function() {
    VG.UI.Container.prototype.show.apply(this, arguments);
    this.update(0);
};

export {SceneLoadingUI};
