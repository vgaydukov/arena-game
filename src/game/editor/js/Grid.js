function Grid(container) {
    VG.SceneEntity.call(this, name);
    var context = this;

    this.autoUpdate = false;

    VG.EventDispatcher.bind("Level.updateParams", this, this.updateParams);

}

Grid.prototype = Object.create(VG.SceneEntity.prototype);
Grid.constructor = Grid;

Grid.prototype.updateParams = function(data) {

    if (!data) data = VG.EventDispatcher.query("Level.getData").level;

    var size = data.size;
    var step = data.step;

    if (this.grid) this.remove(this.grid);

    this.grid = new THREE.GridHelper(size, size / step);
    var offset = (step / 2);
    this.grid.position.x = -offset;
    this.grid.position.z = -offset;

    this.add(this.grid);
};

export {Grid};
