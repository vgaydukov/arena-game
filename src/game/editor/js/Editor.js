import {Grid} from "./Grid";
import {MatrixTool} from "./tools/MatrixTool";
import {EditorModeAssets} from "./tools/EditorModeAssets";
import {EditorModeCells} from "./tools/EditorModeCells";
import {AssetsPallete} from "./AssetsPallete";
import {Level} from "./Level";

function Editor(data) {

    VG.SceneEntity.apply(this, arguments);

    this.level = null;

    this.step = 1;

    this.grid = new Grid();
    this.add(this.grid);

    this.matrixTool = new MatrixTool();

    this.modes = {
        assetsMode: new EditorModeAssets(this),
        cellsMode: new EditorModeCells(this)
    };

    this.activeMode = null;
    this.activateMode("assetsMode");

    this.assetsPallete = new AssetsPallete();

    VG.EventDispatcher.bind("Editor.getData", this, this.getData);

    VG.EventDispatcher.bind("Editor.updateParams", this, this.updateParams);
    VG.EventDispatcher.bind("Editor.saveFile", this, this.saveLevelFile);
    VG.EventDispatcher.bind("Editor.activateMode", this, this.activateMode);

    VG.EventDispatcher.bind("mouse.down", this, this.onMouseDown);
    VG.EventDispatcher.bind("mouse.up", this, this.onMouseUp);
    VG.EventDispatcher.bind("mouse.click", this, this.onMouseClick);
    VG.EventDispatcher.bind("mouse.move", this, this.onMouseMove);

}

Editor.prototype = Object.create(VG.SceneEntity.prototype);
Editor.constructor = Editor;

Editor.prototype.activateMode = function(name) {

    if (this.activeMode) {
        this.activeMode.deactivate();
    }
    this.activeMode = this.modes[name];
    this.activeMode.activate();
};

Editor.prototype.onMouseDown = function(event) {

    this.activeMode.onMouseDown(event);

};

Editor.prototype.onMouseUp = function(event) {

    this.activeMode.onMouseUp(event);

};

Editor.prototype.onMouseClick = function(event) {

    this.activeMode.onMouseClick(event);

};

Editor.prototype.onMouseMove = function(event) {

    this.activeMode.onMouseMove(event);

};

Editor.prototype.createLevel = function(data) {

    if (this.level) {
        this.remove(this.level);
        this.level = null;
    }

    this.level = new Level(data);
    this.add(this.level);

    for (var t in this.modes) {
        this.modes[t].setLevel(this.level);
    }

    this.matrixTool.setLevel(this.level);

    this.grid.updateParams();

};

Editor.prototype.updateParams = function(data) {

    this.step = data.step;

};

Editor.prototype.getData = function() {
    var data = {};
    var levelData = this.level.getData();
    data.step = this.step;
    data.assets = levelData.assets;
    data.level = levelData.level;
    return data;
};

Editor.prototype.saveLevelLocal = function() {
    var data = this.level.getData();
    data = JSON.stringify(data);

    var pako = require("pako"); // eslint-disable-line global-require
    data = pako.deflate(data, {
        to: "string"
    });
    data = btoa(data);

    localStorage.setItem("level", data);
};

Editor.prototype.saveLevelFile = function() {
    var levelData = this.getData();
    var data = {
        assets: levelData.assets,
        level: levelData.level
    };

    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(data));
    var tmpel = document.createElement("a");
    tmpel.setAttribute("href", dataStr);
    tmpel.setAttribute("download", levelData.level.name + ".json");
    tmpel.click();
};

export {Editor};
