import {AssetObject} from "./AssetObject";

function AssetsPallete() {

    this.selectedAsset = null;
    this.assets = {};

    VG.EventDispatcher.bind("AssetsPallete.getAsset", this, this.getAsset);
    VG.EventDispatcher.bind("AssetsPallete.getAssets", this, this.getAssets);
    VG.EventDispatcher.bind("AssetsPallete.selectAsset", this, this.selectAsset);
    VG.EventDispatcher.bind("AssetsPallete.unSelectAsset", this, this.unSelectAsset);
    VG.EventDispatcher.bind("AssetsPallete.getData", this, this.getData);

    VG.EventDispatcher.bind("LevelEditor.loaded", this, this.parseAssets);

    VG.EventDispatcher.bind("keyboard.keydown.Escape", this, this.unSelectAsset);
}

AssetsPallete.prototype = {

    constructor: AssetsPallete,

    getAsset: function(name) {

        return this.assets[name];

    },

    getAssets: function(name) {

        return this.assets;

    },

    selectAsset: function(name) {

        if (!this.assets[name]) return;

        this.selectedAsset = this.assets[name];

        VG.EventDispatcher.send("AssetsPallete.assetSelected", this.selectedAsset);

    },

    unSelectAsset: function() {

        this.selectedAsset = null;

        VG.EventDispatcher.send("AssetsPallete.assetUnSelected");

    },

    parseAssets: function(data) {

        for (var a in data.assets) {
            if (this.assets[a]) continue;
            data.assets[a].name = a;
            if (data.assets[a] instanceof THREE.Object3D) this.assets[a] = new AssetObject(data.assets[a]);
            if (data.assets[a] instanceof Image) this.assets[a] = data.assets[a];

        }
    },

    getData: function() {
        var data = {};
        data.assets = [];

        for (var a in this.assets) {
            if (this.assets[a] instanceof AssetObject) data.assets.push(this.assets[a].mesh.assetUrl);

            if (this.assets[a] instanceof Image) data.assets.push(this.assets[a].assetUrl);
        }

        return data;
    }
};

export {AssetsPallete};
