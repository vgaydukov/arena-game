function EditorMode(editor) {

    this.editor = editor;
    this.matrixTool = editor.matrixTool;
    this.level = null;

}

EditorMode.prototype = {

    constructor: EditorMode,

    activate: function() {

    },

    deactivate: function() {

    },

    onMouseUp: function() {

    },

    onMouseDown: function() {

    },

    onMouseClick: function() {

    },

    onMouseMove: function() {

    },

    setLevel: function(level) {
        this.level = level;
    },

    get levelSize() {
        return this.level.size;
    },

    get levelStep() {
        return this.level.step;
    },

    raycast: function() {

        function findIntersectedMesh(obj) {
            if (obj.parent != context.level.view) return findIntersectedMesh(obj.parent);
            else return obj;
        }

        var raycaster = new THREE.Raycaster();
        var mouse = new THREE.Vector2();

        var intersects, mesh, result, camera, renderer, context;

        return function(event, target) {

            context = this;

            camera = camera || VG.EventDispatcher.query("Engine.get.camera");
            renderer = renderer || VG.EventDispatcher.query("Engine.get.renderer");

            mouse.x = (event.x / renderer.domElement.clientWidth) * 2 - 1;
            mouse.y = -(event.y / renderer.domElement.clientHeight) * 2 + 1;
            raycaster.setFromCamera(mouse, camera);

            intersects = raycaster.intersectObject(target, true);

            for (var i = 0; i < intersects.length; i++) {
                mesh = findIntersectedMesh(intersects[i].object);

                if (!mesh.ignoreRaycaster) {
                    result = intersects[i];
                    result.object = mesh;
                    return result;
                }
            }

        };

    }()
};

export {EditorMode};
