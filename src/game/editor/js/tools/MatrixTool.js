function MatrixTool() {

    this.level = null;
    VG.EventDispatcher.bind("MatrixTool.reCalculateMatrix", this, this.reCalculateMatrix);

}

MatrixTool.prototype = {

    constructor: MatrixTool,

    setLevel: function(level) {
        this.level = level;
    },

    convertPosition: function(position) {

        position.addScalar(this.level.size / 2);

        position.x = Math.round(Math.max(position.x, 0) / this.level.step);
        position.z = Math.round(Math.max(position.z, 0) / this.level.step);

        return position;

    },

    fillForAsset: function() {

        var start = new THREE.Vector3();
        var end = new THREE.Vector3();

        return function(asset, val) {

            asset.computeBbox();

            start.copy(asset.position).sub(asset.bbox.clone().divideScalar(2));
            end.copy(asset.position).add(asset.bbox.clone().divideScalar(2));

            this.fillForVolume(start, end, val);

        };

    }(),

    fillForVolume: function() {

        var min = new THREE.Vector3();
        var max = new THREE.Vector3();

        return function(start, end, val) {

            min.x = Math.min(start.x, end.x);
            min.y = Math.min(start.y, end.y);
            min.z = Math.min(start.z, end.z);

            max.x = Math.max(start.x, end.x);
            max.y = Math.max(start.y, end.y);
            max.z = Math.max(start.z, end.z);

            this.convertPosition(min);
            this.convertPosition(max);

            var cbb = max.sub(min);

            for (var x = 0; x <= cbb.x; x++) {
                for (var z = 0; z <= cbb.z; z++) {
                    this.level.matrix.setCell(min.x + x, min.z + z, val);
                }
            }
        };

    }(),

    reCalculateMatrix: function() {
        this.level.matrix = new VG.LevelMatrix2D((this.level.size / this.level.step), (this.level.size / this.level.step));
        for (var i = this.level.assets.length - 1; i >= 0; i--) {
            this.fillForAsset(this.level.assets[i], 1);
        }
    },

    getCell: function(x, z) {

        return this.level.matrix.getCell(x, z);
    },

    setCell: function(x, z, val) {

        return this.level.matrix.setCell(x, z, val);
    },

    getCellByPosition: function(position) {

        position = position.clone();
        this.convertPosition(position);

        return this.level.matrix.getCell(position.x, position.z);
    },

    setCellByPosition: function(position, value) {

        position = position.clone();
        this.convertPosition(position);

        return this.level.matrix.setCell(position.x, position.z, value);
    }

};

export {MatrixTool};
