import {EditorMode} from "./EditorMode";
import {Terrain} from "../Terrain";

function EditorModeAssets() {
    EditorMode.apply(this, arguments);

    this.selectedAsset = null;
    this.selectedMesh = null;

    var geometry = new THREE.BoxGeometry(1, 1, 1);
    var material = new THREE.MeshBasicMaterial({
        color: "blue",
        opacity: 0.25,
        transparent: true
    });
    this.highlighter = new THREE.Mesh(geometry, material);

    VG.EventDispatcher.bind("AssetsPallete.assetSelected", this, this.assetSelected);
    VG.EventDispatcher.bind("AssetsPallete.assetUnSelected", this, this.assetUnSelected);
    VG.EventDispatcher.bind("Editor.updateMesh", this, this.updateMesh);
    VG.EventDispatcher.bind("keyboard.keydown.Escape", this, this.unSelectMesh);
}

EditorModeAssets.prototype = Object.create(EditorMode.prototype);
EditorModeAssets.constructor = EditorModeAssets;

EditorModeAssets.prototype.onMouseMove = function() {

    var point = new THREE.Vector3();

    return function(event) {
        if (!this.level) return;

        var intersect = this.raycast(event, this.level.view);

        if (intersect) {

            point.set(
                Math.round(Math.abs(intersect.point.x) / this.editor.step) * this.editor.step * Math.sign(intersect.point.x),
                intersect.point.y,
                Math.round(Math.abs(intersect.point.z) / this.editor.step) * this.editor.step * Math.sign(intersect.point.z));

            if (this.selectedAsset) {
                var bbox = this.selectedAsset.bbox;

                if (this.level.collisionPlane == intersect.object) {
                    this.selectedAsset.position.x = point.x;
                    this.selectedAsset.position.y = point.y;
                    this.selectedAsset.position.z = point.z;
                }
                else {
                    this.selectedAsset.position.x = point.x + intersect.face.normal.x * bbox.x / 2;
                    this.selectedAsset.position.y = point.y;
                    this.selectedAsset.position.z = point.z + intersect.face.normal.z * bbox.z / 2;
                }
            }

        }
    };

}();

EditorModeAssets.prototype.onMouseClick = function(event) {

    var intersect = this.raycast(event, this.level.view);

    if (event.button == 0) {

        if (this.selectedAsset) {
            var mesh = this.selectedAsset.clone();
            this.level.addAsset(mesh);
            this.matrixTool.fillForAsset(mesh, 1);
            return;
        }

        if (intersect) {
            mesh = intersect.object;
            if (mesh == this.level.collisionPlane) return;

            this.selectMesh(mesh);
        }
    }
    else if (event.button == 2) {

        if (intersect) {
            mesh = intersect.object;
            if (mesh == this.level.collisionPlane) return;

            this.level.removeAsset(mesh.instance);
            this.matrixTool.fillForAsset(mesh.instance, 0);
        }
    }
};

EditorModeAssets.prototype.assetUnSelected = function() {
    if (this.selectedAsset) this.level.remove(this.selectedAsset);
    this.selectedAsset = null;

    VG.EventDispatcher.send("Editor.meshUnSelected", this.selectedAsset);
};

EditorModeAssets.prototype.assetSelected = function(asset) {
    if (this.selectedAsset) this.level.remove(this.selectedAsset);

    this.selectedAsset = asset;
    this.selectedAsset.view.ignoreRaycaster = true;
    this.level.add(this.selectedAsset);

    VG.EventDispatcher.send("Editor.meshSelected", this.selectedAsset);
};

EditorModeAssets.prototype.selectMesh = function(mesh) {
    if (this.selectedMesh) this.selectedMesh.view.remove(this.highlighter);

    this.selectedMesh = mesh.instance;
    if (!(mesh.instance instanceof Terrain)) {
        this.selectedMesh.computeBbox();
        this.highlighter.scale.set(
            this.selectedMesh.bbox.x / this.selectedMesh.scale.x,
            this.selectedMesh.bbox.y / this.selectedMesh.scale.y,
            this.selectedMesh.bbox.z / this.selectedMesh.scale.z);
        this.highlighter.position.y = (this.selectedMesh.bbox.y / this.selectedMesh.scale.y) / 2;
        this.highlighter.rotation.y = -this.selectedMesh.view.rotation.y;
        this.selectedMesh.view.add(this.highlighter);
    }

    VG.EventDispatcher.send("Editor.meshSelected", this.selectedMesh);
};

EditorModeAssets.prototype.unSelectMesh = function(mesh) {
    if (this.selectedMesh) this.selectedMesh.view.remove(this.highlighter);

    this.selectedMesh = null;
    VG.EventDispatcher.send("Editor.meshUnSelected");

};

EditorModeAssets.prototype.updateMesh = function(data) {
    if (this.selectedAsset) {
        this.selectedAsset.update(data);
        return;
    }

    if (!this.selectedMesh) return;

    this.matrixTool.fillForAsset(this.selectedMesh, 0);
    this.selectedMesh.update(data);
    this.matrixTool.fillForAsset(this.selectedMesh, 1);
};

export {EditorModeAssets};
