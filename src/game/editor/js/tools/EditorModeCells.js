import {EditorMode} from "./EditorMode";

function EditorModeCells() {
    EditorMode.apply(this, arguments);

    this.filledCell = false;
    this.mouseCaptured = false;

    this.startPoint = new THREE.Vector3();
    this.endPoint = new THREE.Vector3();

    this.cellCursor = new THREE.Mesh(
        new THREE.PlaneGeometry(1, 1),
        new THREE.MeshBasicMaterial({
            color: "blue",
            transparent: true,
            opacity: 0.5,
            depthTest: false
        })
    );
    this.cellCursor.visible = false;
    this.cellCursor.rotation.x = -Math.PI / 2;
    this.editor.add(this.cellCursor);

    this.cellsHelper = new THREE.Object3D();
    this.cellsHelper.visible = false;
    this.editor.add(this.cellsHelper);

}

EditorModeCells.prototype = Object.create(EditorMode.prototype);
EditorModeCells.constructor = EditorModeCells;

EditorModeCells.prototype.activate = function() {
    this.cellCursor.visible = true;
    this.cellsHelper.visible = true;
    this.showCells();
};

EditorModeCells.prototype.deactivate = function() {
    this.cellCursor.visible = false;
    this.cellsHelper.visible = false;
};

EditorModeCells.prototype.onMouseDown = function(event) {
    if (event.button != 2) return;

    this.mouseCaptured = true;
    this.filledCell = this.matrixTool.getCellByPosition(this.cellCursor.position);

    this.startPoint.copy(this.cellCursor.position);
    this.endPoint.copy(this.cellCursor.position);
};

EditorModeCells.prototype.onMouseUp = function(event) {
    if (event.button != 2) return;

    if (this.mouseCaptured) {

        this.matrixTool.fillForVolume(this.startPoint, this.endPoint, this.filledCell ? 0 : 1);

        this.showCells();

        this.mouseCaptured = false;
        this.filledCell = false;

        this.onMouseMove(event);
    }
};

EditorModeCells.prototype.onMouseMove = function() {

    var point = new THREE.Vector3();
    var cursorScale = new THREE.Vector3();

    var min = new THREE.Vector3();
    var max = new THREE.Vector3();

    return function(event) {
        if (!this.level) return;

        var intersect = this.raycast(event, this.level.collisionPlane);

        if (!intersect) return;

        point.set(
            Math.round(Math.abs(intersect.point.x) / this.level.step) * this.level.step * Math.sign(intersect.point.x),
            intersect.point.y,
            Math.round(Math.abs(intersect.point.z) / this.level.step) * this.level.step * Math.sign(intersect.point.z));

        if (this.mouseCaptured) {
            this.endPoint.copy(point);

            min.x = Math.min(this.startPoint.x, this.endPoint.x);
            min.y = Math.min(this.startPoint.y, this.endPoint.y);
            min.z = Math.min(this.startPoint.z, this.endPoint.z);

            max.x = Math.max(this.startPoint.x, this.endPoint.x);
            max.y = Math.max(this.startPoint.y, this.endPoint.y);
            max.z = Math.max(this.startPoint.z, this.endPoint.z);

            cursorScale.x = Math.max(max.x - min.x + this.level.step, this.level.step);
            cursorScale.y = Math.max(max.z - min.z + this.level.step, this.level.step);
            cursorScale.z = Math.max(max.y - min.y + this.level.step, this.level.step);

            this.cellCursor.position.copy(min.add(max.clone().sub(min).divideScalar(2)));

            this.cellCursor.scale.copy(cursorScale);

        }
        else {
            this.cellCursor.position.copy(point);
            this.cellCursor.scale.set(this.level.step, this.level.step, this.level.step);
        }
    };

}();

EditorModeCells.prototype.showCells = function() {

    var cellMesh = new THREE.Mesh(
        new THREE.PlaneGeometry(this.level.step, this.level.step),
        new THREE.MeshBasicMaterial({
            color: "red",
            transparent: true,
            opacity: 0.3,
            depthTest: false
        })
    );
    cellMesh.rotation.x = -Math.PI / 2;

    var halfSize = this.level.size / 2;

    while (this.cellsHelper.children.length > 0) this.cellsHelper.remove(this.cellsHelper.children[this.cellsHelper.children.length - 1]);

    for (var x = this.level.matrix.sizeX - 1; x >= 0; x--) {
        for (var y = this.level.matrix.sizeY - 1; y >= 0; y--) {

            if (this.matrixTool.getCell(x, y)) {
                cellMesh = cellMesh.clone();
                cellMesh.ignoreRaycaster = true;
                cellMesh.position.set(
                    x * this.level.step - halfSize,
                    this.level.step / 100,
                    y * this.level.step - halfSize);
                this.cellsHelper.add(cellMesh);
            }
        }
    }
};

export {EditorModeCells};
