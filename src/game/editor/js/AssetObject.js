function AssetObject(mesh) {

    this.view = new THREE.Object3D();
    this.view.add(mesh);
    this.name = mesh.name;
    this.assetUrl = mesh.assetUrl;
    this.view.instance = this;

    this.mesh = mesh;
    this.mesh.castShadow = true;
    this.mesh.receiveShadow = true;
    this.needCalcCells = true;

    this.computeBbox();
}

AssetObject.prototype = {

    constructor: AssetObject,

    computeBbox: function() {
        this.bbox = new THREE.Box3().setFromObject(this.view);
        this.bbox = this.bbox.max.sub(this.bbox.min);
    },

    set position(value) {
        this.view.position.set(value.x, value.y, value.z);
    },

    get position() {
        return this.view.position;
    },

    set rotation(value) {
        this.view.rotation.set(value.x, value.y, value.z);
    },

    get rotation() {
        return this.view.rotation;
    },

    set scale(value) {
        this.view.scale.set(value.x, value.y, value.z);
    },

    get scale() {
        return this.view.scale;
    },

    clone: function() {
        var obj = new this.constructor(this.mesh.clone());
        obj.view = this.view.clone();
        obj.view.instance = obj;
        obj.needCalcCells = this.needCalcCells;
        return obj;
    },

    update: function(data) {

        this.rotation.copy(data.rotation);
        this.position.copy(data.position);
        this.scale.set(data.size, data.size, data.size);
        this.needCalcCells = data.needCalcCells;

        this.computeBbox();

    }
};

export {AssetObject};
