import {Editor} from "../Editor";
import {SceneEditorUI} from "../ui/SceneEditorUI";

function SceneEditor(name) {
    VG.Scene.call(this);

    this.name = "editor";

    var context = this;

    var camera = VG.EventDispatcher.query("Engine.get.camera");
    camera.position.set(0, 50, 50);

    var renderer = VG.EventDispatcher.query("Engine.get.renderer");

    var orbit = new VG.CameraControllerOrbit(camera, renderer.domElement);
    orbit.maxPolarAngle = Math.PI / 2;
    orbit.maxZoom = 100;
    orbit.keyPanSpeed = 20;
    orbit.mouseButtons.ZOOM = -1;
    orbit.mouseButtons.PAN = 1;
    context.add(orbit);

    this.editor = new Editor();
    this.add(this.editor);

    this.ui = new SceneEditorUI();

}
SceneEditor.prototype = Object.create(VG.Scene.prototype);
SceneEditor.constructor = SceneEditor;

SceneEditor.prototype.activate = function(data) {

    VG.Scene.prototype.activate.apply(this, arguments);

    this.editor.createLevel(data);

};
export {SceneEditor};
