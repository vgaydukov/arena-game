import {SceneLoadingUI} from "../ui/SceneLoadingUI";

function SceneLoading(name) {
    VG.Scene.call(this, name);
    this.autoUpdate = false;

    this.name = "loading";

    this.ui = new SceneLoadingUI();

}

SceneLoading.prototype = Object.create(VG.Scene.prototype);
SceneLoading.constructor = SceneLoading;

SceneLoading.prototype.update = function(percent) {

    VG.Scene.prototype.activate(this, arguments);

    this.ui.update(percent);

};

export {SceneLoading};
