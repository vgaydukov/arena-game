import {BaseTerrain} from "./../../js/base/BaseTerrain";

function Terrain(params) {

    BaseTerrain.apply(this, arguments);

    this.params = params;

}

Terrain.prototype = Object.create(BaseTerrain.prototype);
Terrain.constructor = Terrain;

Terrain.prototype.update = function(data) {

    this.rotation.copy(data.rotation);
    this.position.copy(data.position);
    this.scale.set(data.size, data.size, data.size);

};

Terrain.prototype.getData = function() {

    var data = this.params;
    data.pos = this.position.toArray();
    data.scl = this.scale.toArray();
    data.rot = this.rotation.toArray();

    return data;

};

Object.defineProperty(Terrain.prototype, "position", {

    get: function() {
        return this.view.position;
    },

    set: function(value) {
        this.view.position.set(value.x, value.y, value.z);
    }
});

Object.defineProperty(Terrain.prototype, "rotation", {

    get: function() {
        return this.view.rotation;
    },

    set: function(value) {
        this.view.rotation.set(value.x, value.y, value.z);
    }
});

Object.defineProperty(Terrain.prototype, "scale", {

    get: function() {
        return this.view.scale;
    },

    set: function(value) {
        this.view.scale.set(value.x, value.y, value.z);
    }
});

export {Terrain};
