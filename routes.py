from controllers.index import Index
from controllers.user import User
from controllers.room import Room

from controllers.editor import Editor
from controllers.levels import Levels
from controllers.assets import Assets

url_map = [
    (r"/", Index),
	(r"/user", User),
	(r"/room", Room),


    (r"/editor", Editor),
    (r"/levels", Levels),
    (r"/assets/bundle", Assets)
]