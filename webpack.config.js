const path = require("path");
const webpack = require("webpack");

module.exports = [{
    entry: "./src/game/game.js",
    output: {
        path: path.resolve(__dirname, "static/js/dist"),
        filename: "arena_game.js"
    },
    devServer: {
        inline: true,
        port: 10002,
        hot: true,
        host: "0.0.0.0"
    },
    module: {
        rules: [{
            enforce: "pre",
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "eslint-loader"
        }]
    }
},
{
    entry: "./src/game/editor.js",
    output: {
        path: path.resolve(__dirname, "static/js/dist"),
        filename: "arena_level_editor.js"
    },
    module: {
        rules: [{
            enforce: "pre",
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "eslint-loader"
        }]
    },
    resolve: {
        alias: {
            "game": path.join(__dirname, "./src/game/")
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            "$": "jquery",
            "jQuery": "jquery",
            "window.jQuery": "jquery"
        })
    ]

}
];
