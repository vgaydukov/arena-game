import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.options import options
import config
import routes
import os.path

tornado.options.parse_command_line()

application = tornado.web.Application(
        routes.url_map,
        cookie_secret=options.cookie_secret,
        users_secret=options.users_secret,
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        xsrf_cookies=True,
        debug=options.debug
    )
http_server = tornado.httpserver.HTTPServer(application)
http_server.listen(options.port)
tornado.ioloop.IOLoop.current().start()
